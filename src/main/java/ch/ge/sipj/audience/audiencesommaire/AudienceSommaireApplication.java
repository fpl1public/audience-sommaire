package ch.ge.sipj.audience.audiencesommaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AudienceSommaireApplication {

	public static void main(String[] args) {
		SpringApplication.run(AudienceSommaireApplication.class, args);
	}

}
