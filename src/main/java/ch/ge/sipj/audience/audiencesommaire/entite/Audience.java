package ch.ge.sipj.audience.audiencesommaire.entite;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Audience {
    private int id;
    private String procedureNo;
    private String description;
    private String status;
    private int lot_audience_id;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "procedure_no")
    public String getProcedureNo() {
        return procedureNo;
    }

    public void setProcedureNo(String procedureNo) {
        this.procedureNo = procedureNo;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "lot_audience_id")
    public int getLot_audience_id() {
        return lot_audience_id;
    }

    public void setLot_audience_id(int lot_audience_id) {
        this.lot_audience_id = lot_audience_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Audience audience = (Audience) o;
        return id == audience.id && Objects.equals(procedureNo, audience.procedureNo) && Objects.equals(description, audience.description) && Objects.equals(status, audience.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, procedureNo, description, status);
    }
}
