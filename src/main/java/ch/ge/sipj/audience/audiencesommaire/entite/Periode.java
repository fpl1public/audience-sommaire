package ch.ge.sipj.audience.audiencesommaire.entite;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "periode_enum", schema = "public", catalog = "demo")
public class Periode {
    private int id;
    private String valeur;

    public Periode() {
    }

    public Periode(int id, String valeur) {
        this.id = id;
        this.valeur = valeur;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "valeur")
    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Periode periode = (Periode) o;
        return id == periode.id && Objects.equals(valeur, periode.valeur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, valeur);
    }
}
