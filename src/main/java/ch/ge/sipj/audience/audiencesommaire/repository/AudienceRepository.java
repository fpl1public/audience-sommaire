package ch.ge.sipj.audience.audiencesommaire.repository;

import ch.ge.sipj.audience.audiencesommaire.entite.Audience;
import org.springframework.data.repository.CrudRepository;

public interface AudienceRepository extends CrudRepository<Audience, Integer> {
}
