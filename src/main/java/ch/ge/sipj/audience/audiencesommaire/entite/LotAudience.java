package ch.ge.sipj.audience.audiencesommaire.entite;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "lot_audience", schema = "public", catalog = "demo")
public class LotAudience {
    private Integer id;
    private Date jour;
    private String description;
    private Periode periode;
    private List<Audience> audiences;

    public LotAudience() {
    }

    public LotAudience(Date jour, String description, Periode periode) {
        this.jour = jour;
        this.description = description;
        this.periode = periode;
        audiences = new ArrayList<>();
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "jour")
    public Date getJour() {
        return jour;
    }

    public void setJour(Date jour) {
        this.jour = jour;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "lot_audience_id")
    public List<Audience> getAudiences() {
        return audiences;
    }

    public void setAudiences(List<Audience> audiences) {
        this.audiences = audiences;
    }

    @ManyToOne @JoinColumn( name = "periode_id")
    public Periode getPeriode() {
        return periode;
    }

    public void setPeriode(Periode periode) {
        this.periode = periode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LotAudience that = (LotAudience) o;
        return id.equals(that.id) && Objects.equals(jour, that.jour) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, jour,  description);
    }
}
