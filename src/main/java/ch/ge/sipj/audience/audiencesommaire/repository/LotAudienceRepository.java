package ch.ge.sipj.audience.audiencesommaire.repository;

import ch.ge.sipj.audience.audiencesommaire.entite.LotAudience;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LotAudienceRepository extends CrudRepository<LotAudience, Integer> {

    List<LotAudience> findAll();
}
