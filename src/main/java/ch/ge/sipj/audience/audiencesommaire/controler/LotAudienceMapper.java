package ch.ge.sipj.audience.audiencesommaire.controler;

import ch.ge.sipj.audience.audiencesommaire.entite.LotAudience;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
public class LotAudienceMapper {
    public LotAudienceMapper() {
    }

    public LotAudienceDTO map(LotAudience lot) {
        return new LotAudienceDTO(lot.getId(), lot.getJour().toLocalDate().toString(), lot.getPeriode().getValeur(), lot.getDescription());
    }

    public List<LotAudienceDTO> mapList(List<LotAudience> lots) {
        return lots.stream().map(lot -> map(lot)).collect(toList());
    }
}