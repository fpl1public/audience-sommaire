package ch.ge.sipj.audience.audiencesommaire.service;

import ch.ge.sipj.audience.audiencesommaire.controler.LotAudienceDTO;
import ch.ge.sipj.audience.audiencesommaire.controler.LotAudienceMapper;
import ch.ge.sipj.audience.audiencesommaire.entite.LotAudience;
import ch.ge.sipj.audience.audiencesommaire.entite.Periode;
import ch.ge.sipj.audience.audiencesommaire.repository.LotAudienceRepository;
import ch.ge.sipj.audience.audiencesommaire.repository.PeriodeRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Service
public class LotAudienceService {

    private final LotAudienceRepository repository;
    private final PeriodeRepository periodeRepository;
    private final LotAudienceMapper mapper;

    public LotAudienceService(LotAudienceRepository repository, PeriodeRepository periodeRepository, LotAudienceMapper mapper) {
        this.repository = repository;
        this.periodeRepository = periodeRepository;
        this.mapper = mapper;
    }

    public List<LotAudienceDTO> findAll() {
        List<LotAudience> entites = repository.findAll();
        return mapper.mapList(entites);
    }

    public LotAudienceDTO getById(Integer id) {
        LotAudience lotAudience = repository.findById(id)
                                            .orElseThrow(() -> new EntityNotFoundException("Aucun LotAudience d'ID " + id));
        return mapper.map(lotAudience);
    }

    public LotAudienceDTO create(LotAudienceDTO newLotAudienceDTO) {
        Periode periode = checkPeriode(newLotAudienceDTO);
        Date dateAudience = checkDate(newLotAudienceDTO);

        var lot = new LotAudience(dateAudience, newLotAudienceDTO.getDescription(), periode);

        LotAudience newLotAudience = repository.save(lot);
        return mapper.map(newLotAudience);
    }

    private Periode checkPeriode(LotAudienceDTO newLotAudienceDTO) throws IllegalArgumentException{
        Periode periode = periodeRepository.findByValeur(newLotAudienceDTO.getPeriode()).orElseThrow(() -> new IllegalArgumentException("Aucune période ne correspond à la valeur " + newLotAudienceDTO.getPeriode()));
        return periode;
    }

    private Date checkDate(LotAudienceDTO newLotAudienceDTO) throws IllegalArgumentException{
        Date dateAudience = Date.valueOf(newLotAudienceDTO.getJour());
        if (!dateAudience.after(Date.valueOf(LocalDate.now()))) {
            throw new IllegalArgumentException("La date du lot doit être dans le futur !");
        }
        return dateAudience;
    }
}
