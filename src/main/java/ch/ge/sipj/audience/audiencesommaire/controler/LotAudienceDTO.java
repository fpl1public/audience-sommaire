package ch.ge.sipj.audience.audiencesommaire.controler;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LotAudienceDTO {

    @JsonProperty("id")
    private int id;

    @JsonProperty("jour")
    private String jour;

    @JsonProperty("periode")
    private String periode;

    @JsonProperty("description")
    private String description;

    public LotAudienceDTO() {
    }

    public LotAudienceDTO(int id, String jour, String periode, String description) {
        this.id = id;
        this.jour = jour;
        this.periode = periode;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getJour() {
        return jour;
    }

    public String getPeriode() {
        return periode;
    }

    public String getDescription() {
        return description;
    }
}
