package ch.ge.sipj.audience.audiencesommaire.repository;

import ch.ge.sipj.audience.audiencesommaire.entite.Periode;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface PeriodeRepository extends Repository<Periode, Integer> {

    List<Periode> findAll();

    Optional<Periode> findById(Integer integer);

    Optional<Periode> findByValeur(String Valeur);
}
