package ch.ge.sipj.audience.audiencesommaire.controler;

import ch.ge.sipj.audience.audiencesommaire.service.LotAudienceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
public class LotAudienceRestControler {

    private static final Logger logger = LoggerFactory.getLogger(LotAudienceRestControler.class);

    private final LotAudienceService service;

    public LotAudienceRestControler(LotAudienceService service) {
        this.service = service;
    }

    @PostMapping("/lots")
    public LotAudienceDTO newLotAudience(@RequestBody LotAudienceDTO newLotAudience) {
        try {
            LotAudienceDTO lotAudience = service.create(newLotAudience);
            return lotAudience;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping("/lots")
    public List<LotAudienceDTO> findLotAudiences(){
        List<LotAudienceDTO> dtoList = service.findAll();
        System.out.println("dtoList : " + dtoList.toString());
        return dtoList;
    }

    @GetMapping("lots/{id}")
    public LotAudienceDTO getLotAudience(@PathVariable Integer id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            logger.warn("getLotAudience", e);
            throw new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "Le lot d'audience n'existe pas", e);
        }
    }

}
