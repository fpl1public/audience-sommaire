DROP TABLE IF EXISTS AUDIENCE;
DROP TABLE IF EXISTS LOT_AUDIENCE;
DROP TABLE IF EXISTS PERIODE_ENUM;

CREATE TABLE PERIODE_ENUM (
    id SERIAL PRIMARY KEY ,
    valeur varchar(255) NOT NULL
);

CREATE TABLE LOT_AUDIENCE (
                             id SERIAL PRIMARY KEY,
                             jour date NOT NULL,
                             periode_id INT not null,
                             description varchar(255),
                             CONSTRAINT fk_periode FOREIGN KEY (periode_id) REFERENCES PERIODE_ENUM(id)
);

CREATE TABLE AUDIENCE (
                              id SERIAL PRIMARY KEY,
                              procedure_no varchar(20) NOT NULL,
                              description varchar(255),
                              lot_audience_id INT not null,
                              status varchar(10) not null,
                              CONSTRAINT fk_lot FOREIGN KEY (lot_audience_id) REFERENCES LOT_AUDIENCE(id)
);
