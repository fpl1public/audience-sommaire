package ch.ge.sipj.audience.audiencesommaire.rest;

import ch.ge.sipj.audience.audiencesommaire.controler.LotAudienceDTO;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.List.of;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LotAudienceControlerIT {

	private static final Logger logger = LoggerFactory.getLogger(LotAudienceControlerIT.class);

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void findAllLots() {
		String endpoint = "http://localhost:" + port + "/lots";
		ResponseEntity<LotAudienceDTO[]> reponse = restTemplate.getForEntity(endpoint, LotAudienceDTO[].class);
		assertEquals(HttpStatus.OK, reponse.getStatusCode());

		LotAudienceDTO[] lots = reponse.getBody();
		logger.info("Lots retournés par {} : {}", endpoint, lots);
		assertNotNull(lots, "Aucun lot dans la réponse");

		assertEquals(2, lots.length);

		List<Integer> ids = stream(lots).map(LotAudienceDTO::getId).collect(toList());
		logger.info("ids : {}", ids);
		List<String> periodes = stream(lots).map(LotAudienceDTO::getPeriode).collect(toList());
		logger.info("periodes : {}", periodes);
		assertTrue(ids.containsAll(of(1, 2)), "Les lots d'id 1 et 2 sont attendus");
		assertTrue(periodes.containsAll(of("Matin", "Aprem")), "Les 2 lots doivent être Matin ou Après-midi");

	}

}
