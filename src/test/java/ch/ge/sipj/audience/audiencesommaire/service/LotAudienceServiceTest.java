package ch.ge.sipj.audience.audiencesommaire.service;

import ch.ge.sipj.audience.audiencesommaire.controler.LotAudienceDTO;
import ch.ge.sipj.audience.audiencesommaire.controler.LotAudienceMapper;
import ch.ge.sipj.audience.audiencesommaire.entite.Periode;
import ch.ge.sipj.audience.audiencesommaire.repository.LotAudienceRepository;
import ch.ge.sipj.audience.audiencesommaire.repository.PeriodeRepository;
import ch.ge.sipj.audience.audiencesommaire.service.LotAudienceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.util.Optional;

import static java.time.LocalDate.now;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LotAudienceServiceTest {

    @Mock
    private LotAudienceRepository repository;

    @Mock
    private PeriodeRepository periodeRepository;

    private final LotAudienceMapper mapper = new LotAudienceMapper();

    @Test
    void create_periode_doit_exister() {
        // given
        String periodeErronee = "TOTO";
        when(periodeRepository.findByValeur(periodeErronee))
                .thenReturn(Optional.empty());
        LotAudienceService service = new LotAudienceService(repository, periodeRepository, mapper);

        // when
        String demain = Date.valueOf(now().plusDays(1)).toString();

        LotAudienceDTO lotAudienceDTO = new LotAudienceDTO(-1, demain, periodeErronee, "Test");

        // then
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> service.create(lotAudienceDTO));
        assertTrue(exception.getMessage().contains("Aucune période ne correspond à la valeur"));

    }


    @Test
    void create_date_doit_etre_dans_le_futur() {
        // given
        when(periodeRepository.findByValeur("Matin"))
                .thenReturn(Optional.of(new Periode(1, "Matin")));
        LotAudienceService service = new LotAudienceService(repository, periodeRepository, mapper);

        // when
        String aujourdhui = Date.valueOf(now()).toString();
        LotAudienceDTO lotAudienceDTO = new LotAudienceDTO(-1, aujourdhui, "Matin", "Test");

        // then
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> service.create(lotAudienceDTO));
        assertEquals("La date du lot doit être dans le futur !", exception.getMessage());

    }
}